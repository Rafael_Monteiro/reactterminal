const commands = [
 {
     name: 'info',
     response: 'This is a fake terminal, made just for fun!'
 },
 {
    name: 'info --pt',
    response: 'Esse projeto é um terminal falso, feito só por diversão!'
 },
 {
    name: 'info --git',
    response: 'https://gitlab.com/Rafael_Monteiro/reactterminal'
 },
 { 
    name: 'creator',
    response: 'Rafael Monteiro Zancanaro, Computer Technician by IFPR in 2017-2020; Twitter: @RafaoRafoso; GitLab: @Rafael_Monteiro; Reddit:@rafaelmonte'   
},
{
    name: 'commands',
    response: 'info -> information about project; creator -> information about creator; clear -> cleans the terminal'
},
{
    name: 'clear',
    response: ''
},
{
    name: 'fake copyright',
    response: 'O código é livre e pode ser acessado por meio do comando info --git, afinal, o codigo é mais do que algo pra máquinas, é algo pra ajudar pessoas!'
}
]

export default commands;