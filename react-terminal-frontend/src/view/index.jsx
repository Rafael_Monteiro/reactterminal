import React from 'react';
import InputTerminal from '../templates/InputTerminal';
import TerminalResponse from '../templates/TerminalResponse';
import './style/Index.css';
import ResponseHandler from '../logic/ResponseHandler.class';
export default class Index extends React.Component {

    state = {
        responseHandler: new ResponseHandler(),
        isReservedWord: false,
        bufferResponses: []
    }

    componentDidMount() {
        this.state.responseHandler.showCopyrightMessage()
        this.setState({
            bufferResponses: this.showResponses()
        });
    }

    changeCommand = (word) => {
        this.setState({
            isReservedWord: this.state.responseHandler.isReservedWord(word),
        })
    }

    sendCommand = _ => {
        if (this.state.responseHandler.currentCommand === this.state.responseHandler.CLEAR) {
            this.clearBufferAndResetInput()
        } else {
            this.state.responseHandler.sendResponse();
            this.setState({
                bufferResponses: this.showResponses()
            })
        }
    }

    clearBufferAndResetInput = _ => {
        this.setState({
            bufferResponses: this.state.responseHandler.clearAll()
        })
    }
    showResponses = _ => {
        var responses = this.state.responseHandler.responses.map((response, i) => {
            return (
                <li key={i}><TerminalResponse responseText={response}/></li>
            )            
        });
        return responses;
    }

    render() {
        return(
            <div className="index-bg">
                <ul className="index-responses-ul">
                    {this.state.bufferResponses}
                </ul>      
                <InputTerminal reservedWord={this.state.isReservedWord} current={this.state.responseHandler.currentCommand} onChangeCommand={word => this.changeCommand(word) } onEnterPress={_=> this.sendCommand()}></InputTerminal>
            </div>
        ) 
    }
}