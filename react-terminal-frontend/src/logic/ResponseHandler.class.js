import Commands from '../static/commands.static';

export default class ResponseHandler {
    commands = Commands;
    currentCommand = "";
    suggestionCommand = "";
    EMPTY_STRING = "";
    responses = [];
    CLEAR='clear';
    COPY_MESSAGE='fake copyright';
    GIT_COMMAND='info --git';

    showCopyrightMessage() {
        this.setCommand(this.COPY_MESSAGE);
    }

    setCommand(comm) {
        this.currentCommand = comm;
        this.sendResponse();
    }


    sendResponse() {

        if (this.commands.filter(command => command.name === this.currentCommand).length > 0) {
            return this.setCommandResponse();
        }
        return this.setErrorResponse();
    }

    setErrorResponse() {
        this.responses.push(`'${this.currentCommand}' nao foi reconhecido na base de comandos do sistema, experimente usar 'commands' para ver os comandos`);
        this.currentCommand = "";
        return this.responses;
    }

    setCommandResponse() {
        let filteredCommand = this.commands.filter(command => command.name === this.currentCommand);
        this.responses.push(filteredCommand[0].response);
        this.isGitCommand(filteredCommand[0].response);
        this.currentCommand = "";
        return this.responses;
    }

    isGitCommand(response) {
        if (this.currentCommand === this.GIT_COMMAND) {
            window.open(response, '_blank');
        }
    }

    isReservedWord(word) {
        this.currentCommand = word;
        // this.setSuggestionBasedInCurrentCommand(); //TODO
        return (this.commands.filter(command => command.name === word).length > 0);
    }

    // setSuggestionBasedInCurrentCommand() { //TODO
    //     let suggestionCommand = this.commands.filter(command => command.name.includes(this.currentCommand));
    //     if (suggestionCommand.length > 0 && this.currentCommand !== this.EMPTY_STRING) {
    //         this.suggestionCommand = suggestionCommand[0].name;
    //     }
    // }

    clearAll() {
        this.currentCommand = this.EMPTY_STRING;
        this.responses = [];
        return [];
    }
}