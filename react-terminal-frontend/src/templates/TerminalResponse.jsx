import React from 'react';
import './style/TerminalResponse.css';
const TerminalResponse = props => {

    return (
            <div className="term-response-line">
                <span className="term-response-text">
                    {props.responseText}
                </span>
            </div>
    )
}

export default TerminalResponse;