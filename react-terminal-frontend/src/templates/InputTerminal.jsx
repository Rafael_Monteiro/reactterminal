import './style/InputTerminal.css';
const InputTerminal = props => {

    const ENTER_KEY = "Enter";
    
    return(
        <div className="term-input-line">
            <span className="term-input-line-desc">User@React-OS:~$</span>
            <input type="text" className="term-input-line-input" value={props.current} onChange={evt => props.onChangeCommand(evt.target.value)}
            style= {{
                color: props.reservedWord && props.current !== ""? 'yellow': 'white'
            }}
            onKeyPress={evt => {if(evt.key === ENTER_KEY && props.current.replaceAll(/\s/g,'') !== "") {props.onEnterPress()}} }
            />
        </div>
    )
}

export default InputTerminal;