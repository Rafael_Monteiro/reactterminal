const fileReader = require('fs');
const path = require('path');
const getDetailedInfo = () => {
    return {
        command: 'info --verbose',
        response: fileReader.readFileSync(path.resolve(__dirname, '../staticFiles/info.txt'), {encoding: 'utf8'}, (error) => {
            if (error) throw Error('Erro ao ler arquivo de informações');
        }).toString()
    }
}

const getInfo = () => {
    return {
        command: 'info',
        response: 'This is a fake terminal, made just for fun!'
    }
}

const getVersion = () => {
    return {
        command: 'info --version',
        response: getVersionFromJSON(fileReader.readFileSync(path.resolve(__dirname, '../package.json'), {encoding: 'utf8'}).toString())
    }
}

const detailedInfoAboutProject = () => {
    return {
        command: 'sysInfo --fingerprint',
        response: JSON.stringify(fileReader.readFileSync(path.resolve(__dirname, '../package-lock.json'), {encoding: 'utf8'}))
    }
}

function getVersionFromJSON(jsonString) {
    return JSON.parse(jsonString).version;
}

module.exports = {
    getInfo: getInfo,
    getDetailedInfo: getDetailedInfo,
    getVersion: getVersion,
    detailedInfoAboutProject: detailedInfoAboutProject
}

