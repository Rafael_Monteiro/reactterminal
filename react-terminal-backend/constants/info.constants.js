const infoSpecifiers = [
    '--verbose',
    '--tmm'
]

const VERBOSE = '--verbose';

const VERSION = '--version';

const VERSION_REDUCED = '-v';

function isVerbose(specification) {
    return infoSpecifiers.includes(specification);
}
module.exports = {
    isVerbose: isVerbose
}


