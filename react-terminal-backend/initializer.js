const SERVER_PORT = '8081';

const express = require('express');

const app = express();

app.listen(SERVER_PORT, (error) => {
    if (error) {
        throw Error(`Erro ao iniciar o servidor na porta ${SERVER_PORT}`);
    }
    console.log(`Servidor iniciado na porta ${SERVER_PORT}`);
});


module.exports = {
    serverPort: SERVER_PORT,
    api: app
}