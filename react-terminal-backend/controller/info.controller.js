const initializer = require('../initializer.js');
const infoConstants = require('../constants/info.constants.js');
const service = require('../service/info.service.js');
const api = initializer.api;

api.get('/commands/basic/info/:specifier?', (req, res) => {
    if (infoConstants.isVerbose(req.params.specifier)) {
        return res.status(200).send(service.getDetailedInfo());
    }
    return res.status(200).send(service.getInfo());
});


api.get('/commands/basic/sysinfo/--version', (req, res) => {
    res.status(200).send(service.getVersion())
})

api.get('/commands/basic/sysinfo/-version/--verbose', (req, res) => {
    res.status(200).send(service.getDetailedVersion())
})

api.get('/commands/basic/sysinfo/--fingerprint', (req, res) => {
    res.status(200).send(service.detailedInfoAboutProject())
})

api.get('/commands/basic/sysinfo/--git', (req, res) => {
    res.status(200).send({command: 'sysinfo --git', response: 'Doing...'});
})

api.get('/commands/basic/i/want/some/easter/eggs', (req, res) => {
    res.status(201).send('<h3>Sabia que pessoas curiosas movem o mundo? Continue assim!<h3>')
})